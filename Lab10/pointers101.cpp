#include <iostream>

using namespace std;

int main()
{
    int myvariable = 10;
    int anothervariable  = 20;
    int pointervariable = &myvariable;

    cout << "myvariable = " << myvariable << endl;
    cout << "anothervariable = " << anothervariable << endl;
    cout << "address of anothervariable " << &anothervariable << endl;
    cout << "address of myvariable " << &myvariable << endl;    
    
    cout << "pointervariable points to" << *pointervariable << endl;
    cout <<"which means pointervariavble = "<< pointervariable <<  endl;
    
    pointervariable = & anothervariable;
    
    cout << "pointervariable points to" << *pointervariable << endl;
    cout <<"which means pointervariavble = "<< pointervariable <<  endl;
    return 0;
}