#include <iostream>
#include <string>

using namespace std;

void sort2(double* p, double* q)
{
    double temp=0;
     
    if(*p > *q)
    {
        temp = *p;
        *p = *q;
        *q = temp;
    }
}


int main()

{
    double x=20;
    double y=10;
    
    cout << "(Before Func.) x = " << x << " y = " << y << endl;
    
    sort2( &x, &y) ;
    
    cout << "(After Func.) x = " << x << " y = " << y << endl;
    
    return 0;

}