#include <iostream>
#include <string>

using namespace std;

int main()

{
   double harry_account = 0;
   double joint_account = 2000;
   double* account_pointer = &harry_account;
 
   *account_pointer = 1000; //initial deposit
   *account_pointer = *account_pointer -100; //Widthaw
   
   cout << "Balance : << " << *account_pointer<< endl;
   
   account_pointer = &joint_account;
   *account_pointer = *account_pointer -100;
   
   cout << "Balance : << " << *account_pointer<< endl;
   return 0;
}