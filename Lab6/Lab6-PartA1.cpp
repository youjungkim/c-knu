#include <iostream>
#include <cmath>
#include <string>

using namespace std;

int main()

{
    double sum=0;
    int i=2;
    while (i<= 100)
    {
        sum += i;
        i += 2;
    }
    cout << "a. The sum of all even numbers between 2 and 100 : " << sum << endl;

    
    sum=0; i=1;
    while (i<= 100)
    {
        sum += sqrt(i);
        i ++;
    }
    cout << "b. The sum of all squares between 1 and 100 : " << sum << endl;
    
    sum=0; i=20;
    while (i<= 220)
    {
        sum += pow(i, 2);
        i ++;
    }
    cout << "c. All powers of 2 from 20 up to 220 : " << sum << endl;
    
    sum=0; 
    int a=0, b=0;
    cout << "Enter a : " ;
    cin >> a;
    cout << "Enter b : " ;
    cin >> b;
    
    while ( a<=b )
    {
        if(a%2 != 0)
        {
            sum += a;
        }
            a ++;
    }
    cout << "d. The sum of all odd numbers between a and b : " << sum << endl ;
    
    sum=0;
    string number;
    cout << "Enter the random number : " ;
    cin >>  number ;
    
    for(int i=0 ; i< number.length() ; i++ )
    {
        if (number.at(i) %2 != 0)
        {
            sum += number.at(i) ;
        }
    }
    cout << "e.The sum of all odd digits of an input : " << sum << endl;
    
    
    
    return 0;
}