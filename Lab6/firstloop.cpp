#include <iostream>
#include <string>

using namespace std;

int main()

{
    cout << "Enter an interest rate : " << endl;
    double rate;
    cin >> rate;
    
    cout << "Enter initial amount : " << endl;
    double initial_balance;
    cin >> initial_balance;
    
    cout << "Enter total amount : " << endl;
    double target;
    cin >> target;
    
    double balance =  initial_balance;
    /*int year =0;
    
    for( ; balance < target ; year++ )
    {
        balance = balance* rate;
    }*/
    
    int year = 0;
    double interest = 0;
    
    while (balance <target)
    {
        year++;
        interest = balance + rate/100;
        balance = balance + interest;
    }
    
    cout << "THE INVESTMENT DOUBLED AFTER" << year <<"years" << endl;
    
    return 0;

}