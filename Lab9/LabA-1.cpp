#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()

{
    const int SIZE = 10; 
    int numbers[];
    
    srand (time(0));
    
    for (int i=0; i<SIZE; i++)
    {
        numbers[i] = rand()%RAND_MAX*100;    
    }
    
    cout << "Every element at an even index:"<< endl;
    for (i=0; i<SIZE; i++)
    {
        if (i%2==0)   
        {
            cout << "index" << i << " = " << numbers[i] << " " ;
        }
    }
    
    
    
    cout << "Every even element:" << endl;
    for (i=0; i<SIZE; i++)
    {
        if (numbers[i]%2==0)   
        {
            cout << "index" << i << " = " << numbers[i] << " " ;
        }
    }
    
    cout << "All elements in reverse order:" << endl;
    for (i=SIZE-1; i>=0; i--)
    {
        cout << "index" << i << " = " << numbers[i] << " " ;
    }
    
    cout<< "Only the first and last element:" << endl;
    cout<< "The first element :" << "index 0 " << " = " << numbers[0] << endl;
    cout<< "The last element  :" << "index 9 " << " = " << numbers[9] << endl;
    
    return 0;

}