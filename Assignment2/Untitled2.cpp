#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;


/*This is a program generating a palindrom.
A palindrome is a word that is spelled the same way forwards and backwards.
If you enter the length of palindrome, then The program will generate a valid palindrome.
And if you want to terminate this program, enter a non-positive value.*/

int main()

{
    srand (time(0));
    
    int length=0;
    int i=0;
    string pal= "";
    char asciiChar;
    
    cout << "Enter the length of palindrome : ";
    
    
    cin >> length ;
    
    while (length>0)
    {
        
        if(length %2 != 0)
        {
            for(i=0; i <= (length/2); i++)
            {
                int asciiVal = rand()%26 +97;
                asciiChar = asciiVal;
                cout << asciiChar;
                pal += asciiChar;
            }
            
            if (length != 1)
            {
                for (i=(length/2)-1; i>=0; i--)
                {
                    cout << pal.substr(i, 1);
                }
            }
        }
        else
        {
            for(i=0; i < (length/2); i++)
            {
                int asciiVal = rand()%26 +97;
                asciiChar = asciiVal;
                cout << asciiChar;
                pal += asciiChar;
            }
            
      
            for (i=(length/2)-1; i>=0; i--)
            {
                cout << pal.substr(i, 1);
            }
            
        }
        cout <<endl;
        
        pal = "";
        cout << "Enter the length of palindrome : ";
        cin >> length ;
    }
    cout << "This program has just finished. Thankyou." << endl;
    return 0;
}