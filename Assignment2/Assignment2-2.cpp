#include <iostream>
#include <string>

using namespace std;

/*This is a program that get some sentence and test that is a valid palindrome.
A palindrome is a word that is spelled the same way forwards and backwards.
If you enter some palindrome, then The program will check.*/

int main()

{
    string pal;
    int result = 0;
    
    cout << "Enter a palindrome : " ;
    getline (cin, pal);
    
    int length = pal.length();
    
    string ans;
    cout << "Do you want to ignore case?";
    cin >> ans;
    
    
    if(ans == "N" || ans == "n")
    {
        for (int i=0; i<length/2; i++)
        {
            if ( pal.substr(i,1) == pal.substr(length-i-1,1) )
            {
                result = 1;
            }
            else
            {
                result = 2;
                break;
            }    
        }
    }
    else
    {
         for (int i=0; i<length/2; i++)
        {
            char asciiChar1 = pal.at(i);
            char asciiChar2 = pal.at(length-i-1);
            
            int asciiVal1 = asciiChar1;
            int asciiVal2 = asciiChar2;
            
            if ( (asciiVal1 ==  asciiVal2) || ( asciiVal1 == asciiVal2 + 32 ) || ( asciiVal1 == asciiVal2 - 32 ))
            {
                result = 1;
            }
            else
            {
                result = 2;
                break;
            }    
        }
    }
    switch (result)
    {
        case 1:
        cout << "This is a valid palindrome." << endl;
        break;
        
        case 2:
        cout << "This is a invalid palindrome."<< endl;
        break;
        
        defalt:
        break;
    }
    
    return 0;
}