#include "LabA-1algorithm.h"
#include <iostream>

using namespace std;

double larger ( double num1, double num2)
{
    if (num1 > num2)
        return num1;
    else
        return num2;
}




int main()

{
  double a, b, c, d, e = 0;
  cout << "Enter three number : ";
  cin >> a >> b >> c ;
  d = larger(a, b);
  e = larger(b, c);
  cout << "a. the larger of the first two inputs : " << larger(a, b) << endl;
  cout << "b. the larger of the last two inputs  : " << larger(b, c) << endl;
  cout << "c. the largest of all three inputs    : " << larger(d, e) << endl;
  
  return 0;

}