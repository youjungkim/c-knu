#include <iostream>
#include <string>

using namespace std;

int main()

{
    string Frist_name, Second_name, name;
    const double hourly_wage = 9.25;
    int hours=0;
    double regular_wage=0, extra_wage=0, total_salary=0;
    
    cout << "First name : ";
    cin >> Frist_name ;
    cout << "Second name : ";
    cin >> Second_name ;
    name = Frist_name + " " + Second_name;
    
    cout << "Worked hours? : ";
    cin >> hours ;
    
    
    regular_wage = hours*hourly_wage;
    
    if (hours > 40)
    {
        extra_wage = (hours-40)*hourly_wage*1.5;    
    }
    total_salary = regular_wage + extra_wage;
    
    cout << "\t<paychek>" << endl;
    cout << "name : " << name << endl;
    cout << "total salary : $" << total_salary << endl;
    
    return 0;

}