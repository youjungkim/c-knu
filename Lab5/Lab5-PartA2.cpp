#include <iostream>
#include <cmath>

using namespace std;

int main()

{
    float first=0, second=0;
    
    cout << "Enter two floating-point numbers : ";
    cin >> first >> second ;
    
    if (0<=abs(first-second) && abs(first-second)<0.01)
    {
        cout << "There are the same up to two decimal places." << endl;
    }
    else
    {
        cout << "They are different." << endl;
    }
    
    return 0;

}