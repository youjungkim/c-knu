#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

/*option 1
The user guesses 5 numbers and get the result.
@par the answer code
@par the size of guess
@ret the vector of guess deta 
*/
vector<int> Guess(int code[], int size)
{
    int correct=0, right=0, count=0;
    int* guess[12];
    for(int i=0; i<12; i++) //initialize
    {
        guess[i] = new int[size + 2];
        for (int j=0; j<size+2; j++) {guess[i][j] = 0;}
    }
    
    for(int i=0; i<12; i++)
    {
        correct=0, right=0;  
        cout << "Guess five numbers:" ; //User input
        cin >> guess[i][0]>>guess[i][1]>>guess[i][2]>>guess[i][3]>> guess[i][4] ;
        
        for(int j=0; j<size; j++)  //The computer compute result.
        {
            if (code[j] == guess[i][j]) { right++; }
            else
            {
                for(int k=0; k<size; k++)
                {
                    if( (k!=j) &&( code[k] == guess[i][j] ) ) { correct++; }
                }
            }
        }
        guess[i][5]=correct; guess[i][6]=right; count++;
        cout << "Result : " << correct << "numbers correct, " << right << "in right position" << endl;
        
        if(correct == 5)
        {
            cout << "Congratulations! You're guess is right!"<<endl; break;
        }
        if( (correct !=5) && (i==11))
        {
            cout << "You used all 12 chances. The game is over"<< endl; break;
        }
    }
    return guess;
}

/*option 2
Print the history deta about user's guess. User can review them.
@par the history of data
@par the answer code
@par the size of numbers*/
void print_guess(vector<int> guess,int* code,int size)
{
    cout << "Secret Code is " ;
    for(int i=0; i<size; i++)
    {
        if(i>0) {cout << ",";}
        cout << *(code+i);
    }
    
    for(int i=0; i<12; i++)
    {
        cout << "Guess" << i << ": ";
        for(int j=0; j<size; j++)
        {
            if(i>0) {cout << ",";}
            cout << guess[i]+j;
        }
        cout << guess[i]+5 << "numbers correct, " << guess[i]+6 << "in right position" << endl;
    }        
}

/*option 3
Game start refreshly.
The computer randomly chooses 5 numbers between 0-9
and stores them in an vector.
@par the size of numbers
@ret The vector of code*/
void Newgame(int* code, int size)
{
    srand(time(0));
    for(int i=0; i<size; i++)
    {
        *(code+i) = rand()%10;
    }
}

int main()
{
    const int SIZE = 5;
    vector<int> guess;
    int code[SIZE]={0,};
    int menu=0, i;
    bool go =true;
    
    while(go)
    {
        //Choose option of the Menu.
        cout << "\t*Menu*"<< endl;
        cout << "1. (G)uess " <<endl <<"2. (R)eview Guesses" << endl <<"3. (N)ew game" << endl << "4. (Q)uit game" << endl;
        cout << "Enter the number : " ;
        cin >> menu;
    
        if(menu==1)
        {
            guess = Guess(code, SIZE);
        }
        else if(menu==2)
        {
            print_guess(guess, code, SIZE);
        }
        else if(menu==3)
        {
            if(code)
            {
                for(i=0; i<SIZE; i++){ code[i]=0;}
            }
            Newgame(code, SIZE);
            while(guess.size())
            {
                guess.pop_back();
            }
        }
        else if (menu==4)
        {
            go=false; break;
        }
        else
        {
            cout << "You entered wrong number!" << endl;
        }
    }    
    cout<< "The game has just terminated.Thank you." << endl;
    
    return 0;

}