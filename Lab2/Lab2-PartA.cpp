#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    cout << "enter the length for x distance: " ;
    double x_distance;
    cin >> x_distance;
    
    cout << "Enter the length for y distance : ";
    double y_distance;
    cin >> y_distance;
    
    cout << "Enter the length of segment 1" ;
    double s1_length;
    cin >> s1_length;
    
    cout << "Enter speed on the road in KMPH" ;
    double s1_speed;
    cin >> s1_speed;
    
    cout << "Enter speed off the road in KMPH" ;
    double segment2_speed;
    cin >> segment2_speed;
    
    double s1_time = s1_length/s1_speed;
    double segment2_length = sqrt(pow(x_distance, 2) + pow (y_distance-s1_length, 2) );
    double segment2_time = segment2_length/ segment2_speed;
    
    cout << "Time for segment1 = " << s1_time << endl ;
    cout << "Time for segment2  = " << segment2_time << endl ;

    double totaltime = s1_time + segment2_time;
    

    return 0;
}